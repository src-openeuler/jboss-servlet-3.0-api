Name:             jboss-servlet-3.0-api
Version:          1.0.2
Release:          11
Summary:          Java Servlet 3.0 API
License:          CDDL-1.0
Url:              http://www.jboss.org
Source0:          https://github.com/jboss/jboss-servlet-api_spec/archive/jboss-servlet-api_3.0_spec-1.0.2.Final.tar.gz
Source1:          cddl.txt
BuildRequires:    maven-local mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:    mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:    mvn(org.apache.maven.plugins:maven-source-plugin) mvn(org.jboss:jboss-parent:pom:)
BuildArch:        noarch

%description
The Java Servlet 3.0 API classes.

%package javadoc
Summary: Javadoc for jboss-servlet-3.0-api

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n jboss-servlet-api_spec-jboss-servlet-api_3.0_spec-1.0.2.Final
cp %{SOURCE1} .

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE README cddl.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE README cddl.txt

%changelog
* Mon May 9 2022 baizhonggui <baizhonggui@h-partners.com> - 1.0.2-11
- modify license identifier

* Thu Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.2-10
- Package init
